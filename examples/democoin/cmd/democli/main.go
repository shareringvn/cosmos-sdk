package main

import (
	"os"

	"github.com/spf13/cobra"

	"github.com/tendermint/tmlibs/cli"

	"bitbucket.org/shareringvn/cosmos-sdk/client"
	"bitbucket.org/shareringvn/cosmos-sdk/client/keys"
	"bitbucket.org/shareringvn/cosmos-sdk/client/lcd"
	"bitbucket.org/shareringvn/cosmos-sdk/client/rpc"
	"bitbucket.org/shareringvn/cosmos-sdk/client/tx"

	"bitbucket.org/shareringvn/cosmos-sdk/version"
	authcmd "bitbucket.org/shareringvn/cosmos-sdk/x/auth/client/cli"
	bankcmd "bitbucket.org/shareringvn/cosmos-sdk/x/bank/client/cli"
	ibccmd "bitbucket.org/shareringvn/cosmos-sdk/x/ibc/client/cli"

	"bitbucket.org/shareringvn/cosmos-sdk/examples/democoin/app"
	"bitbucket.org/shareringvn/cosmos-sdk/examples/democoin/types"
	coolcmd "bitbucket.org/shareringvn/cosmos-sdk/examples/democoin/x/cool/client/cli"
	powcmd "bitbucket.org/shareringvn/cosmos-sdk/examples/democoin/x/pow/client/cli"
	simplestakingcmd "bitbucket.org/shareringvn/cosmos-sdk/examples/democoin/x/simplestake/client/cli"
)

// rootCmd is the entry point for this binary
var (
	rootCmd = &cobra.Command{
		Use:   "democli",
		Short: "Democoin light-client",
	}
)

func main() {
	// disable sorting
	cobra.EnableCommandSorting = false

	// get the codec
	cdc := app.MakeCodec()

	// TODO: setup keybase, viper object, etc. to be passed into
	// the below functions and eliminate global vars, like we do
	// with the cdc

	// add standard rpc, and tx commands
	rpc.AddCommands(rootCmd)
	rootCmd.AddCommand(client.LineBreak)
	tx.AddCommands(rootCmd, cdc)
	rootCmd.AddCommand(client.LineBreak)

	// add query/post commands (custom to binary)
	// start with commands common to basecoin
	rootCmd.AddCommand(
		client.GetCommands(
			authcmd.GetAccountCmd("acc", cdc, types.GetAccountDecoder(cdc)),
		)...)
	rootCmd.AddCommand(
		client.PostCommands(
			bankcmd.SendTxCmd(cdc),
		)...)
	rootCmd.AddCommand(
		client.PostCommands(
			ibccmd.IBCTransferCmd(cdc),
		)...)
	rootCmd.AddCommand(
		client.PostCommands(
			ibccmd.IBCRelayCmd(cdc),
			simplestakingcmd.BondTxCmd(cdc),
		)...)
	rootCmd.AddCommand(
		client.PostCommands(
			simplestakingcmd.UnbondTxCmd(cdc),
		)...)
	// and now democoin specific commands
	rootCmd.AddCommand(
		client.PostCommands(
			coolcmd.QuizTxCmd(cdc),
			coolcmd.SetTrendTxCmd(cdc),
			powcmd.MineCmd(cdc),
		)...)

	// add proxy, version and key info
	rootCmd.AddCommand(
		client.LineBreak,
		lcd.ServeCommand(cdc),
		keys.Commands(),
		client.LineBreak,
		version.VersionCmd,
	)

	// prepare and add flags
	executor := cli.PrepareMainCmd(rootCmd, "BC", os.ExpandEnv("$HOME/.democli"))
	executor.Execute()
}
