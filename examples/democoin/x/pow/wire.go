package pow

import (
	"bitbucket.org/shareringvn/cosmos-sdk/wire"
)

// Register concrete types on wire codec
func RegisterWire(cdc *wire.Codec) {
	cdc.RegisterConcrete(MsgMine{}, "pow/Mine", nil)
}
