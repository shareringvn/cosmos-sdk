package types

import wire "bitbucket.org/shareringvn/cosmos-sdk/wire"

// Register the sdk message type
func RegisterWire(cdc *wire.Codec) {
	cdc.RegisterInterface((*Msg)(nil), nil)
	cdc.RegisterInterface((*Tx)(nil), nil)
}
